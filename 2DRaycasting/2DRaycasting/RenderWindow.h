#ifndef RENDERWINDOW_H
#define RENDERWINDOW_H

#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <string>
#include <vector>

/*#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm> //std::min and std::max*/

//#undef main //->this shit is so it doesn't get the undefined link error.

class RenderWindow
{
public:
	RenderWindow();
	~RenderWindow();

	//variables to initiate:
	bool Init(int screenWidth, int screenHeight, const char* windowName = "SDL_Window");
	bool BeginMedia();

	//methods:
	void Close();
	void RenderClear();
	void RenderCopy(SDL_Texture* texture, SDL_Rect* clip = nullptr, SDL_Rect* rect = nullptr);
	void RenderPresent();
	//this one can also be used to tile by setting the rectangle
	void RenderSetViewport(SDL_Rect* rect);

	//used to load images:
	SDL_Texture* LoadTexture(const std::string& file);
	
	//Color sturcts
	struct ColorRGB8bit;
	struct ColorRGB
	{
		int r;
		int g;
		int b;

		ColorRGB(Uint8 r, Uint8 g, Uint8 b);
		ColorRGB(const ColorRGB8bit &color);
		ColorRGB();
	};

	friend ColorRGB operator+(const ColorRGB &color, const ColorRGB &color2);
	friend ColorRGB operator-(const ColorRGB& color, const ColorRGB& color2);
	friend ColorRGB operator*(const ColorRGB& color, int a);
	friend ColorRGB operator*(int a, const ColorRGB& color);
	friend ColorRGB operator/(const ColorRGB& color, int a);
	friend bool operator==(const ColorRGB& color, const ColorRGB& color2);
	friend bool operator!=(const ColorRGB& color, const ColorRGB& color2);

	static const ColorRGB RGB_Black();
	static const ColorRGB RGB_Red();
	static const ColorRGB RGB_Green();
	static const ColorRGB RGB_Blue();
	static const ColorRGB RGB_Yellow();
	static const ColorRGB RGB_White();
	static const ColorRGB RGB_Purple();

	struct ColorRGB8bit
	{
		Uint8 r;
		Uint8 g;
		Uint8 b;

		ColorRGB8bit(Uint8 r, Uint8 g, Uint8 b);
		ColorRGB8bit(const ColorRGB &color);
		ColorRGB8bit();
	};

	//using old surface shit:
	//void Redraw();
	void Cls(const ColorRGB &color);

	//2D Shapes:
	//bool verLine(int x, int y1, int y2, int h, int w, const ColorRGB &color);

	bool RenderVerticalLine(int x, int y1, int y2, int h, int w, const ColorRGB &color);

	void RenderBuffer(int screenWidth, int screenHeight, Uint32 *buffer);

	void RenderLine(int x1, int x2, int y1, int y2);
	void SetRenderColor(const ColorRGB &color);

	//Reading an image into integers
	void LoadFile(std::vector<unsigned char>& buffer, const std::string& filename);//designed for loading files from hard disk in an std::vector
	int LoadImage(std::vector<Uint32>& out, unsigned long& w, unsigned long& h, const std::string& filename);
	int DecodePNG(std::vector<unsigned char>& out_image, unsigned long& image_width, unsigned long& image_height, const unsigned char* in_png, size_t in_size, bool convert_to_rgba32 = true);
	int DecodePNG(std::vector<unsigned char>& out_image_32bit, unsigned long& image_width, unsigned long& image_height, const std::vector<unsigned char>& in_png);

private:
	//window that stuff will be rendered into:
	SDL_Window *window = nullptr;

	//the renderer:
	SDL_Renderer *renderer = nullptr;

	//associated surface:
	SDL_Surface *surface = nullptr;

	//texture:
	SDL_Texture *texture = nullptr;
};
#endif