#include <cmath>
#include <string>
#include <vector>
#include <iostream>

#include "RaycastUntextured.h"
#include "RaycastTextured.h"

//Screen dimension constants:
const int SCREEN_WIDTH = screenWidthT;
const int SCREEN_HEIGHT = screenHeightT;

int main(int argc, char *args[])
{
	//RaycastUntextured raycastUntextured = RaycastUntextured();
	//raycastUntextured.Update(SCREEN_WIDTH, SCREEN_HEIGHT);

	RaycastTextured raycastTextured = RaycastTextured();
	raycastTextured.Update(SCREEN_WIDTH, SCREEN_HEIGHT);

    return 0;
}