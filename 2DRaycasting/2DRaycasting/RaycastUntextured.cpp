#include "RaycastUntextured.h"

RaycastUntextured::RaycastUntextured()
{
}


RaycastUntextured::~RaycastUntextured()
{
}

void RaycastUntextured::Update(const int width, const int height)
{
	bool quit = false;

	if (!renderWindow.Init(width, height, "2D Raycaster"))
	{
		printf("failed to initialize SDL window\n");
	}
	else
	{
		//main loop:
		while (!quit)
		{
			//clear renderer:
			renderWindow.SetRenderColor(RenderWindow::RGB_Black());
			renderWindow.RenderClear();

			//raycasting loop:
			for (int x = 0; x < width; x++)
			{
				//calculate ray position and direction:
				double cameraX = 2 * x / double(width) - 1;// x-coordinate in camera space. Normalizes between -1 and 1, 0 is the center of the camera.
				double rayDirX = dirX + planeX * cameraX;
				double rayDirY = dirY + planeY * cameraX;

				//which box of the map we're in, basically iterators of the world map array.
				int mapX = int(posX);
				int mapY = int(posY);

				//length of the ray from the current position to the next x-side or y-side:
				double sideDistX;
				double sideDistY;

				//length of the ray from one x-side or y-side to the next x-side or y-side:
				double deltaDistX = abs(1 / rayDirX);
				double deltaDistY = abs(1 / rayDirY);
				double perpWallDist;//->used to calculate the length of the ray.

									//what direction to step x-direction or y-direction, either -1 or +1
				int stepX;
				int stepY;

				int hit = 0;//was there a wall hit?
				int side; //was there an NS or WE wall hit?

						  //calculate step and initial side distance:
				if (rayDirX < 0)
				{
					stepX = -1;
					sideDistX = (posX - mapX) * deltaDistX;
				}
				else
				{
					stepX = 1;
					sideDistX = (mapX + 1.0 - posX) * deltaDistX;
				}

				if (rayDirY < 0)
				{
					stepY = -1;
					sideDistY = (posY - mapY) * deltaDistY;
				}
				else
				{
					stepY = 1;
					sideDistY = (mapY + 1.0 - posY) * deltaDistY;
				}

				//perform DDA algorithm:
				while (hit == 0)
				{
					//jump to next map square, OR in x-direction, OR in y-direction:
					if (sideDistX < sideDistY)
					{
						sideDistX += deltaDistX;
						mapX += stepX;
						side = 0;
					}
					else
					{
						sideDistY += deltaDistY;
						mapY += stepY;
						side = 1;
					}
					//check if a ray has hit a wall:
					if (worldMapNoTex[mapX][mapY] > 0)
						hit = 1;
				}

				//Calculate the distance projected on camera direction (Euclidian distance will give fisheye effect):
				if (side == 0)
					perpWallDist = (mapX - posX + (1 - stepX) / 2) / rayDirX;//number of squares crossed in the x-direction
				else
					perpWallDist = (mapY - posY + (1 - stepY) / 2) / rayDirY;

				//calculate height of line to draw on screen:
				int lineHeight = (int)(height / perpWallDist);

				//calculate lowest and highest pixel to fill in current stripe:
				int drawStart = -lineHeight / 2 + height / 2;
				if (drawStart < 0)
					drawStart = 0;

				int drawEnd = lineHeight / 2 + height / 2;
				if (drawEnd >= height)
					drawEnd = height - 1;

				RenderWindow::ColorRGB color;
				switch (worldMapNoTex[mapX][mapY])
				{
				case 1: color = RenderWindow::RGB_Green();
					break;
				case 2: color = RenderWindow::RGB_White();
					break;
				case 3: color = RenderWindow::RGB_Purple();
					break;
				case 4: color = RenderWindow::RGB_Red();
					break;
				case 5: color = RenderWindow::RGB_Blue();
					break;

				default: color = RenderWindow::RGB_Yellow();
					break;
				}

				//give x and y sides different brightness
				if (side == 1)
				{
					color = color / 2;
				}

				//draw the pixels of the stripe as a vertical line:
				renderWindow.RenderVerticalLine(x, drawStart, drawEnd, height, width, color);
			}
			
			//a crosshair for shits and giggles:
			//renderWindow.RenderVerticalLine(width / 2, (height / 2) - 10, (height / 2) + 10, height, width, RenderWindow::RGB_Yellow());

			//timing for inputs and FPS counter:
			oldTime = time;
			time = SDL_GetTicks();
			double frameTime = (time - oldTime) / 1000.0;//the time this frame has taken in seconds.
			//std::cout << "FPS: " << (1.0 / frameTime) << std::endl;

														 //present the renderer:
			renderWindow.RenderPresent();

			//speed modifiers:
			double moveSpeed = frameTime * 50.0;// squares/second
			double rotSpeed = frameTime * 50.0;//radians/second

			//INPUT STUFF!
			while (SDL_PollEvent(&e) != 0)
			{
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
				//pressing a key:
				else if (e.type == SDL_KEYDOWN)
				{
					double oldDirX;
					double oldPlaneX;
					
					switch (e.key.keysym.sym)
					{
					case SDLK_w:
						if (worldMapNoTex[int(posX + dirX * moveSpeed)][int(posY)] == 0
							|| worldMapNoTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posX += dirX * moveSpeed;
						if (worldMapNoTex[int(posX)][int(posY + dirY * moveSpeed)] == 0
							|| worldMapNoTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posY += dirY * moveSpeed;
						break;
					case SDLK_s:
						if (worldMapNoTex[int(posX - dirX * moveSpeed)][int(posY)] == 0
							|| worldMapNoTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posX -= dirX * moveSpeed;
						if (worldMapNoTex[int(posX)][int(posY - dirY * moveSpeed)] == 0
							|| worldMapNoTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posY -= dirY * moveSpeed;
						break;
					case SDLK_a:
						//both camera direction and camera plane must be rotated
						oldDirX = dirX;
						dirX = dirX * cos(rotSpeed) - dirY * sin(rotSpeed);
						dirY = oldDirX * sin(rotSpeed) + dirY * cos(rotSpeed);
						oldPlaneX = planeX;
						planeX = planeX * cos(rotSpeed) - planeY * sin(rotSpeed);
						planeY = oldPlaneX * sin(rotSpeed) + planeY * cos(rotSpeed);
						break;
					case SDLK_d:
						//both camera direction and camera plane must be rotated
						oldDirX = dirX;
						dirX = dirX * cos(-rotSpeed) - dirY * sin(-rotSpeed);
						dirY = oldDirX * sin(-rotSpeed) + dirY * cos(-rotSpeed);
						oldPlaneX = planeX;
						planeX = planeX * cos(-rotSpeed) - planeY * sin(-rotSpeed);
						planeY = oldPlaneX * sin(-rotSpeed) + planeY * cos(-rotSpeed);
						break;
					case SDLK_ESCAPE:
							quit = true;
						break;
					}
				}
			}
		}
	}

	//close SDL stuff:
	renderWindow.Close();
}