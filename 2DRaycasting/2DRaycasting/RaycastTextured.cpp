#include "RaycastTextured.h"

//Uint32 buffer[screenWidthT][screenHeightT]; // y-coordinate first because it works per scanline
Uint32 buffer[screenHeightT][screenWidthT];

RaycastTextured::RaycastTextured()
{
}

RaycastTextured::~RaycastTextured()
{
}

void RaycastTextured::Update(const int screenWidth, const int screenHeight)
{
	bool quit = false;

	if (!renderWindow.Init(screenWidth, screenHeight, "2D Raycaster"))
	{
		printf("failed to initialize SDL window\n");
	}
	else
	{
		vector<Uint32> texture[8];//two dimensional
		for (int i = 0; i < 8; i++)
		{
			texture[i].resize(texWidth * texHeight);//the area.
		}

		//load texture images (+1 on the map):	
		unsigned long tw, th;
		renderWindow.LoadImage(texture[0], tw, th, "Pics/door_1.png");
		renderWindow.LoadImage(texture[1], tw, th, "Pics/wood.png");
		renderWindow.LoadImage(texture[2], tw, th, "Pics/purplestone.png");
		renderWindow.LoadImage(texture[3], tw, th, "Pics/greystone.png");
		renderWindow.LoadImage(texture[4], tw, th, "Pics/bluestone.png");
		renderWindow.LoadImage(texture[5], tw, th, "Pics/mossy.png");
		renderWindow.LoadImage(texture[6], tw, th, "Pics/redbrick.png");
		renderWindow.LoadImage(texture[7], tw, th, "Pics/colorstone.png");

		//main loop:
		while (!quit)
		{
			//clear renderer:
			renderWindow.RenderClear();

			//raycasting loop:
			for (int x = 0; x < screenWidth; x++)
			{
				//optional, give it a CCTV kinda look, by skipping some vertical lines.
				if (x % 2 == 0)
					continue;

				//calculate ray position and direction:
				double cameraX = 2 * x / double(screenWidth) - 1;// x-coordinate in camera space. Normalizes between -1 and 1, 0 is the center of the camera.
				double rayDirX = dirX + planeX * cameraX;
				double rayDirY = dirY + planeY * cameraX;

				//which box of the map we're in, basically iterators of the world map array.
				int mapX = int(posX);
				int mapY = int(posY);

				//length of the ray from the current position to the next x-side or y-side:
				double sideDistX;
				double sideDistY;

				//length of the ray from one x-side or y-side to the next x-side or y-side:
				double deltaDistX = abs(1 / rayDirX);
				double deltaDistY = abs(1 / rayDirY);
				double perpWallDist;//->used to calculate the length of the ray.

				//what direction to step x-direction or y-direction, either -1 or +1
				int stepX;
				int stepY;

				int hit = 0;//was there a wall hit?
				int side; //was there an NS or WE wall hit?

				//calculate step and initial side distance:
				if (rayDirX < 0)
				{
					stepX = -1;
					sideDistX = (posX - mapX) * deltaDistX;
				}
				else
				{
					stepX = 1;
					sideDistX = (mapX + 1.0 - posX) * deltaDistX;
				}

				if (rayDirY < 0)
				{
					stepY = -1;
					sideDistY = (posY - mapY) * deltaDistY;
				}
				else
				{
					stepY = 1;
					sideDistY = (mapY + 1.0 - posY) * deltaDistY;
				}

				//perform DDA algorithm:
				while (hit == 0)
				{
					//jump to next map square, OR in x-direction, OR in y-direction:
					if (sideDistX < sideDistY)
					{
						sideDistX += deltaDistX;
						mapX += stepX;
						side = 0;
					}
					else
					{
						sideDistY += deltaDistY;
						mapY += stepY;
						side = 1;
					}
					//check if a ray has hit a wall:
					if (worldMapTex[mapX][mapY] > 0)
						hit = 1;
				}

				//Calculate the distance projected on camera direction (Euclidian distance will give fisheye effect):
				if (side == 0)
					perpWallDist = (mapX - posX + (1 - stepX) / 2) / rayDirX;//number of squares crossed in the x-direction
				else
					perpWallDist = (mapY - posY + (1 - stepY) / 2) / rayDirY;

				//calculate screenHeightT of line to draw on screen:
				int lineHeight = (int)(screenHeight / perpWallDist / heightMultiplier);

				//calculate lowest and highest pixel to fill in current stripe:
				int drawStart = -lineHeight / 2 + screenHeight / 2;
				if (drawStart < 0)
					drawStart = 0;

				int drawEnd = lineHeight / 2 + screenHeight / 2;
				if (drawEnd >= screenHeight)
					drawEnd = screenHeight - 1;

				//texturing calculations:
				int texNum = worldMapTex[mapX][mapY] - 1;//subtract 1 so texture 0 can be used.

				//calculate new value of wallX
				double wallX;//where exactly the wall was hit.
				
				if (side == 0)
					wallX = posY + perpWallDist * rayDirY;
				else
					wallX = posX + perpWallDist * rayDirX;

				wallX -= floor(wallX);

				//x-coor of the texture:
				int texX = int(wallX * double(texWidth));
				if (side == 0 && rayDirX > 0)
					texX = texWidth - texX - 1;
				if (side == 1 && rayDirY < 0)
					texX = texWidth - texX - 1;

				for (int y = drawStart; y < drawEnd; y++)
				{
					int d = y * 256 - screenHeight * 128 + lineHeight * 128; //ints avoid floats.
					int texY = ((d * texHeight) / lineHeight) / 256;//Find a way to rewrite this division to speed up.

					//Uint32 color = texture[texNum][texHeight * texY + texX];//tex num, the number of sprites starting from 0.
					Uint32 color = texture[texNum][texWidth * texY + texX];

					//make color darker for y-sides: R, G and B byte each divided through two with a SHIFT and an AND
					if (side == 1)
						color = (color >> 1) & 8355711;//011111110111111101111111 in Binary
					
					buffer[y][x] = color;
				}

				//floor casting:
				//x, y position of the floor texel at the bottom of the wall.
				double floorXWall, floorYwall;

				//4 different wall directions are possible:
				if (side == 0 && rayDirX > 0)
				{
					floorXWall = mapX;
					floorYwall = mapY + wallX;
				}
				else if (side == 0 && rayDirX < 0)
				{
					floorXWall = mapX + 1.0;
					floorYwall = mapY + wallX;
				}
				else if (side == 1 && rayDirY > 0)
				{
					floorXWall = mapX + wallX;
					floorYwall = mapY;
				}
				else
				{
					floorXWall = mapX + wallX;
					floorYwall = mapY + 1.0;
				}

				double distWall, distPlayer, currentDist;

				distWall = perpWallDist;
				distPlayer = 0.0;

				//draw end is less than 0 when the int overflows
				if (drawEnd < 0)
					drawEnd = screenHeight;

				//draw the floor from drawEnd to the bottom of the screen:
				for (int y=drawEnd + 1; y < screenHeight; y++)
				{
					currentDist = screenHeight / (2.0 * y - screenHeight);

					double weight = (currentDist - distPlayer) / (distWall - distPlayer);

					double currentFloorX = weight * floorXWall + (1.0 - weight) * posX;
					double currentFloorY = weight * floorYwall + (1.0 - weight) * posY;

					int floorTexX, floorTexY;
					floorTexX = int(currentFloorX * texWidth) % texWidth;
					floorTexY = int(currentFloorY * texHeight) % texHeight;

					//int checkerBoardPattern = (int(currentFloorX + currentFloorY)) % 2;
					int checkerBoardPattern = (int(currentFloorX) + int(currentFloorY)) % 2;
					int floorTexture;
					if (checkerBoardPattern == 0)
						floorTexture = 3;
					else
						floorTexture = 5;

					//floor:
					buffer[y][x] = (texture[floorTexture][texWidth * floorTexY + floorTexX] >> 1) & 8355711;
					//ceiling, symetrical
					buffer[screenHeight - y][x] = texture[1][texWidth * floorTexY + floorTexX];
				}
			}

			renderWindow.RenderBuffer(screenWidth, screenHeight, buffer[0]);
			for (int i = 0; i < screenWidth; i++)
			{
				for (int j = 0; j < screenHeight; j++)
				{
					buffer[j][i] = 0;
				}
			}

			//timing for inputs and FPS counter:
			oldTime = time;
			time = SDL_GetTicks();
			double frameTime = (time - oldTime) / 1000.0;//the time this frame has taken in seconds.
			//cout << "FPS: " << (1.0 / frameTime) << endl;
			

			//present the renderer:
			renderWindow.RenderPresent();

			//speed modifiers:
			double moveSpeed = frameTime * 5.0;// squares/second
			//double rotSpeed = frameTime * 3.0;//radians/second
			
			//INPUT STUFF!
			double oldDirX;
			double oldPlaneX;
			float actualRotationSpeed = frameTime * mouseSensitivity;
			
			if (mouseRelX > 1.1)
				mouseRelX = 1;
			else if (mouseRelX < -1.1)
				mouseRelX = -1;
			else
				mouseRelX = 0;

			actualRotationSpeed = mouseRelX * frameTime * mouseSensitivity;

			oldDirX = dirX;
			dirX = dirX * cos(-actualRotationSpeed) - dirY * sin(-actualRotationSpeed);
			dirY = oldDirX * sin(-actualRotationSpeed) + dirY * cos(-actualRotationSpeed);
			oldPlaneX = planeX;
			planeX = planeX * cos(-actualRotationSpeed) - planeY * sin(-actualRotationSpeed);
			planeY = oldPlaneX * sin(-actualRotationSpeed) + planeY * cos(-actualRotationSpeed);

			while (SDL_PollEvent(&e) != 0)
			{
				if (e.type == SDL_QUIT)
				{
					quit = true;
				}
				//pressing a key:
				else if (e.type == SDL_KEYDOWN)
				{
					switch (e.key.keysym.sym)
					{
						//Strafing:
					case SDLK_w:
						//a simple "collision" check, 0 is nothing, 1 is a door:
						if (worldMapTex[int(posX + dirX * moveSpeed)][int(posY)] == 0
							|| worldMapTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posX += dirX * moveSpeed;
						if (worldMapTex[int(posX)][int(posY + dirY * moveSpeed)] == 0)
							posY += dirY * moveSpeed;
						break;
					case SDLK_s:
						if (worldMapTex[int(posX - dirX * moveSpeed)][int(posY)] == 0
							|| worldMapTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posX -= dirX * moveSpeed;
						if (worldMapTex[int(posX)][int(posY - dirY * moveSpeed)] == 0)
							posY -= dirY * moveSpeed;
						break;
					case SDLK_d:
						if (worldMapTex[int(posX + planeX * moveSpeed)][int(posY)] == 0
							|| worldMapTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posX += planeX * moveSpeed;
						if (worldMapTex[int(posX)][int(posY + planeY * moveSpeed)] == 0)
							posY += planeY * moveSpeed;
						break;
					case SDLK_a:
						if (worldMapTex[int(posX - planeX * moveSpeed)][int(posY)] == 0
							|| worldMapTex[int(posX + dirX * moveSpeed)][int(posY)] == 1)
							posX -= planeX * moveSpeed;
						if (worldMapTex[int(posX)][int(posY - planeY * moveSpeed)] == 0)
							posY -= planeY * moveSpeed;
						break;
						//Rotating the camera left and right:
					/*case SDLK_LEFT:
						//both camera direction and camera plane must be rotated
						oldDirX = dirX;
						dirX = dirX * cos(rotSpeed) - dirY * sin(rotSpeed);
						dirY = oldDirX * sin(rotSpeed) + dirY * cos(rotSpeed);
						oldPlaneX = planeX;
						planeX = planeX * cos(rotSpeed) - planeY * sin(rotSpeed);
						planeY = oldPlaneX * sin(rotSpeed) + planeY * cos(rotSpeed);
						break;
					case SDLK_RIGHT:
						//both camera direction and camera plane must be rotated
						oldDirX = dirX;
						dirX = dirX * cos(-rotSpeed) - dirY * sin(-rotSpeed);
						dirY = oldDirX * sin(-rotSpeed) + dirY * cos(-rotSpeed);
						oldPlaneX = planeX;
						planeX = planeX * cos(-rotSpeed) - planeY * sin(-rotSpeed);
						planeY = oldPlaneX * sin(-rotSpeed) + planeY * cos(-rotSpeed);
						break;*/
					case SDLK_ESCAPE:
							quit = true;
						break;		
					/*case SDLK_SPACE:
						if (!SDL_GetRelativeMouseMode())
							SDL_SetRelativeMouseMode(SDL_TRUE);
						else
							SDL_SetRelativeMouseMode(SDL_FALSE);
						break;*/
					}
				}
				else if (e.type == SDL_KEYUP)
				{
					switch (e.key.keysym.sym)
					{
					case SDLK_d:

						break;
					}
				}
				//moving the mouse:
				else if (e.type == SDL_MOUSEMOTION)
				{
					mouseMotion = true;
					if (!mouseFirst)
					{
						mouseRelX = e.motion.xrel;
					}
					else
					{
						//to reset to 0:
						mouseFirst = false;
						mouseRelX = 0;
					}				
				}
			}
		}
	}
	
	//close SDL stuff:
	renderWindow.Close();
}