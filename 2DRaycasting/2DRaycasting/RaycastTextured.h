#ifndef RAYCASTTEXTURED_H
#define RAYCASTTEXTURED_H

#define screenWidthT 640
#define screenHeightT 480
#define texWidth 64
#define texHeight 64
#define mapWidthT 24
#define mapHeightT 24

#include "RenderWindow.h"
//#include <vector>
#include <iostream>
#include <string>

using namespace std;

class RaycastTextured
{
public:
	RaycastTextured();
	~RaycastTextured();

	void Update(const int screenWidth, const int screenHeight);

private:
	int worldMapTex[mapWidthT][mapHeightT] =
	{
	{ 4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5 },
	{ 4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5 },
	{ 4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5 },
	{ 4,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,2,2,2,2,2,2,2,2 },
	{ 4,0,0,4,0,0,0,0,4,0,0,0,0,0,0,0,2,0,0,0,0,0,0,2 },
	{ 4,0,0,4,4,0,4,4,4,0,0,0,0,0,0,0,1,0,0,0,0,0,0,2 },
	{ 4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,2,2,2 },
	{ 4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,4,0,2,0,0,0,0,2,5 },
	{ 4,0,0,4,0,0,0,0,0,0,0,0,0,0,0,4,0,2,2,0,0,2,2,5 },
	{ 4,4,4,4,4,0,0,0,0,0,4,4,4,4,4,4,0,0,2,0,0,2,0,5 },
	{ 5,5,5,5,5,5,5,1,5,5,5,5,5,5,5,0,0,0,2,0,0,2,0,5 },
	{ 5,0,0,0,0,5,0,0,0,5,0,0,0,0,5,0,0,0,2,0,0,2,0,5 },
	{ 5,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,2,0,0,2,0,5 },
	{ 5,0,0,0,0,5,0,0,0,5,0,0,0,0,5,0,0,0,2,0,0,2,0,5 },
	{ 5,0,0,0,0,5,0,0,0,5,0,0,0,0,5,0,0,0,2,0,0,2,0,5 },
	{ 5,5,5,5,5,5,0,0,0,5,5,5,5,5,5,0,0,0,2,0,0,2,0,5 },
	{ 5,0,0,0,0,5,0,0,0,5,0,0,0,0,5,0,0,0,2,0,0,0,0,5 },
	{ 5,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,2,0,0,2,2,2 },
	{ 5,0,0,0,0,5,0,0,0,5,0,0,0,0,5,0,0,0,2,0,0,1,0,2 },
	{ 5,5,5,5,5,5,0,0,0,5,5,5,5,5,5,0,0,0,2,0,0,2,0,2 },
	{ 5,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,2,0,0,2,2,2 },
	{ 5,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,2,0,0,2,0,5 },
	{ 5,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,2,2,2,2,0,5 },
	{ 5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,2,2,2,2,5 }
	};

	//initialize renderwindow.
	RenderWindow renderWindow = RenderWindow();

	//event handler:
	SDL_Event e;

	//double posX = 22, posY = 11.5; //x and y start position of the player.
	double posX = 17, posY = 3; //x and y start position of the player.
	double dirX = -1, dirY = 0.0; //initial direction vector.
	double planeX = 0.0, planeY = 0.66; //the 2d raycaster version of the camera plane. Around 66� FOV for 0.66

	double time = 0.0; //time of the current frame.
	double oldTime = 0.0; //time of the previous frame.	

	double heightMultiplier = planeY / 0.66;

	//mouse stuff:
	bool mouseFirst = true;
	bool mouseMotion = false;
	double mouseRelX = 0.0;
	double mouseSensitivity = 3.0;
};
#endif